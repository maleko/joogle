require 'joogle/user_interfaces/base'

module Joogle
  module UserInterfaces
    class SearchMenu
      def display_search_term(description:, choices:)
        base.select(description: description, choices: choices)
      end

      def display_search_value(description:)
        base.ask(description: description)
      end

      def display_search_result(result:)
        base.say(description: result)
      end

      private

      def base
        @base ||= Base.new
      end
    end
  end
end
