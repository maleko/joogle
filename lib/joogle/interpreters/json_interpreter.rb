require 'json'

module Joogle
  module Interpreters
    class JSONInterpreter
      def parse(filename:)
        contents = File.read filename
        JSON.parse(contents, symbolize_names: true)
      end
    end
  end
end

