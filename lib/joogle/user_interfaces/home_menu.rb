require 'joogle/user_interfaces/base'

module Joogle
  module UserInterfaces
    class HomeMenu
      def prompt(description:, choices:)
        base.prompt description: description, choices: choices
      end

      private

      def base
        @base ||= Base.new
      end
    end
  end
end
