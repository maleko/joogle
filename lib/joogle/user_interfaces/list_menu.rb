require 'joogle/user_interfaces/base'

module Joogle
  module UserInterfaces
    class ListMenu
      def list_fields(description:, fields:)
        display_text = formatted_description(description: description, fields: fields)
        base.say(description: display_text)
      end

      private

      def formatted_description(description:, fields:)
        "\n" + [line, description, line, fields].flatten.join("\n")
      end

      def line
        "-" * 10
      end

      def base
        @base ||= Base.new
      end
    end
  end
end
