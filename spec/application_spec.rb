require 'spec_helper'

module Joogle
  RSpec.describe Application do
    let(:controller) { instance_double Controllers::MainController, display_home_menu: nil }

    before do
      allow(Controllers::MainController).to receive(:new).and_return controller
    end

    describe '.new' do
      subject { described_class.new }

      it 'initializes the main controller' do
        subject
        expect(Controllers::MainController).to have_received(:new)
      end
    end

    describe '#run' do
      subject { described_class.new.run }

      it 'displays the initial menu' do
        subject
        expect(controller).to have_received(:display_home_menu)
      end
    end

  end
end
