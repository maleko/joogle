require 'spec_helper'

module Joogle
  module UserInterfaces
    RSpec.describe Base do
      let(:base) { described_class.new }
      let(:prompt) { instance_double TTY::Prompt, say: nil, expand: answer, ask: answer, select: answer }
      let(:description) { 'lorem ipsum' }
      let(:choices) { [{}] }

      let(:answer) { 'whatever' }

      before do
        allow(TTY::Prompt).to receive(:new).and_return prompt
      end

      subject { question }

      describe '.prompt' do
        let(:question) { base.prompt(description: description, choices: choices) }

        it 'instantiates an instance of TTY::Prompt' do
          subject
          expect(TTY::Prompt).to have_received(:new)
        end

        it 'sends an expand request' do
          subject
          expect(prompt).to have_received(:expand).with(description, choices)
        end

        it 'returns the answer' do
          is_expected.to eq answer
        end
      end

      describe '.say' do
        let(:question) { base.say(description: description) }

        it 'instantiates an instance of TTY::Prompt' do
          subject
          expect(TTY::Prompt).to have_received(:new)
        end

        it 'sends an ask request' do
          subject
          expect(prompt).to have_received(:say).with(description)
        end
      end

      describe '.ask' do
        let(:question) { base.ask(description: description) }

        it 'instantiates an instance of TTY::Prompt' do
          subject
          expect(TTY::Prompt).to have_received(:new)
        end

        it 'sends an ask request' do
          subject
          expect(prompt).to have_received(:ask).with(description)
        end

        it 'returns the answer' do
          is_expected.to eq answer
        end
      end

      describe '.select' do
        let(:question) { base.select(description: description, choices: choices) }

        it 'instantiates an instance of TTY::Prompt' do
          subject
          expect(TTY::Prompt).to have_received(:new)
        end

        it 'sends an ask request' do
          subject
          expect(prompt).to have_received(:select).with(description, choices)
        end

        it 'returns the answer' do
          is_expected.to eq answer
        end
      end
    end
  end
end


