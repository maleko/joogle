module Joogle
  module Repositories
    class Repository
      class UnknownFieldException < Exception; end

      def initialize(resource:)
        @resource = resource
      end

      def search(field_name:, value:)
        validate(field_name: field_name)
        elements.select { |element| includes?(field_value: element[field_name], value: value) }
      end

      def list_field_names
        field_names
      end

      def load(resource: @resource, interpreter: Interpreters::JSONInterpreter.new)
        @elements ||= interpreter.parse(filename: resource)
        self
      end

      private

      attr_reader :resource
      attr_reader :elements

      def includes?(field_value:, value:)
        field_value.to_s.include?(value.to_s)
      end

      def validate(field_name:)
        raise UnknownFieldException unless field_names.include? field_name
      end

      def field_names
        @field_names ||= elements.map(&:keys).flatten.uniq!
      end
    end
  end
end
