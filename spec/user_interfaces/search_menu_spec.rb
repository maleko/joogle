require 'spec_helper'

module Joogle
  module UserInterfaces
    RSpec.describe SearchMenu do
      let(:base) { instance_double Base, select: answer, ask: answer, say: nil}
      let(:answer) { 'whatever' }
      let(:description) { 'Lorem ipsum dolor sit amet' }
      let(:choices) { [{}] }
      let(:search_menu) { described_class.new }

      before do
        allow(Base).to receive(:new).and_return base
      end

      describe '#display_search_term' do
        subject { search_menu.display_search_term(description: description, choices: choices) }

        it 'presents a selection based on the choices provided' do
          subject
          expect(base).to have_received(:select).with(description: description, choices: choices)
        end

        it 'returns the answer' do
          is_expected.to eq answer
        end
      end

      describe '#display_search_value' do
        subject { search_menu.display_search_value(description: description) }

        it 'presents a question' do
          subject
          expect(base).to have_received(:ask).with(description: description)
        end

        it 'returns the answer' do
          is_expected.to eq answer
        end
      end

      describe '#display_search_result' do
        subject { search_menu.display_search_result(result: description) }

        it 'presents a question' do
          subject
          expect(base).to have_received(:say).with(description: description)
        end
      end
    end
  end
end
