require 'spec_helper'

module Joogle
  module Repositories
    RSpec.describe Repository do

      let(:filename) { "#{Dir.pwd}/spec/fixtures/users.json" }
      let(:json) { JSON.parse(File.read(filename), symbolize_keys: true) }
      let(:repository) { described_class.new(resource: filename) }
      let(:elements) { [user_a, user_b, user_c] }
      let(:user_a) { { _test1: 1, test2: ['wolf', 'bark', 'howl'], test3: 'meow' } }
      let(:user_b) { { _test1: 2, test2: ['chirp', 'squeak', 'hoot'], test3: 'hiss' } }
      let(:user_c) { { _test1: 3, test2: ['squeal'], test3: 'moo', test4: 'surprise!'} }

      subject { repository }

      describe '#search' do
        subject { repository.search(field_name: field_name, value: value) }

        before do
          allow(repository).to receive(:elements).and_return elements
        end

        context 'valid field name given' do
          let(:field_name) { :test3 }

          context 'existing value' do

            context 'string value' do
              let(:value) { 'hiss' }

              it 'returns the user with the search value' do
                is_expected.to eq [user_b]
              end
            end

            context 'integer value' do
              let(:field_name) { :_test1 }
              let(:value) { '3' }

              it 'returns the user with the search value' do
                is_expected.to eq [user_c]
              end
            end

            context 'array search' do
              let(:field_name) { :test2 }
              let(:value) { 'hoot' }

              it 'returns the user with the array element' do
                is_expected.to eq [user_b]
              end
            end
          end

          context 'non-existent value' do
            let(:value) { 'speak' }

            it 'returns an empty array if none was found' do
              is_expected.to eq []
            end
          end
        end

        context 'invalid field name given' do
          let(:field_name) { :bleah }
          let(:value) { 'blink' }

          it 'raises an error' do
            expect{ subject }.to raise_error Joogle::Repositories::Repository::UnknownFieldException
          end
        end
      end

      describe '#list_field_names' do
        subject { repository.list_field_names }

        before do
          allow(repository).to receive(:elements).and_return elements
        end

        it 'lists all the keys' do
          is_expected.to eq [:_test1, :test2, :test3, :test4]
        end
      end

      describe '#load' do
        let(:interpreter) { instance_double Interpreters::JSONInterpreter }
        let(:results) { [result] }
        let(:result) { { test1: :a, test2: :b } }

        subject { repository.load(resource: filename)  }

        before do
          allow(Interpreters::JSONInterpreter).to receive(:new).and_return interpreter
          allow(interpreter).to receive(:parse).and_return results
        end

        it 'passes the file name to the interpreter' do
          subject
          expect(interpreter).to have_received(:parse).with(filename: filename)
        end

        it 'returns itself' do
          expect(subject).to eq repository
        end

        context 'substituted interpreter' do
          let(:substitute_interpreter) { double 'WhateverInterpreter', parse: nil }

          subject { repository.load(resource: filename, interpreter: substitute_interpreter) }

          it 'uses the substituted intepreter' do
            subject
            expect(substitute_interpreter).to have_received(:parse).with(filename: filename)
          end
        end
      end

    end
  end
end
