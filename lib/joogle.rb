require 'joogle/version'
require 'joogle/user_interfaces'
require 'joogle/repositories'
require 'joogle/interpreters'
require 'joogle/controllers'
require 'awesome_print'

module Joogle
  class << self
    def application
      @application ||= Joogle::Application.new
    end
  end

  class Application
    def initialize
      @controller = Controllers::MainController.new
    end

    def run
      controller.display_home_menu
    end

    private

    attr_reader :controller
  end
end
