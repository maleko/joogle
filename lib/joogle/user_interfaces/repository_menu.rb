require 'joogle/user_interfaces/base'

module Joogle
  module UserInterfaces
    class RepositoryMenu
      def display_repositories_list(description:, choices:)
        base.select(description: description, choices: choices)
      end

      private

      def base
        @base ||= Base.new
      end
    end
  end
end
