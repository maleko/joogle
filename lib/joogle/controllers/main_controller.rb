require 'joogle/config'

module Joogle
  module Controllers
    class MainController

      def initialize
        init_menus
        init_repositories
      end

      def display_home_menu
        decision = home_menu.prompt(
          description: settings[:home_menu][:description],
          choices: settings[:home_menu][:choices]
        )
        display_repository_menu if decision == 'search'
        display_list_menu if decision == 'list'
      end

      def display_repository_menu
        @repository = repository_menu.display_repositories_list(
          description: settings[:repository_menu][:description],
          choices: repositories
        )
        display_search_menu
      end

      def display_search_menu
        @field_name = search_menu.display_search_term(
          description: settings[:search_menu][:search_term][:description],
          choices: repository.list_field_names
        )
        @value = search_menu.display_search_value(
          description: settings[:search_menu][:search_value][:description]
        )
        display_search_result
      end

      def display_search_result
        result = search(field_name: field_name, value: value)
        search_menu.display_search_result(result: JSON.pretty_generate(result))
        display_home_menu
      end

      def display_list_menu
        repositories.each do |name, rep|
          list_menu.list_fields(description: name, fields: rep.list_field_names)
        end
        display_home_menu
      end

      private

      attr_reader :home_menu, :repository_menu, :search_menu, :list_menu
      attr_reader :user_repository, :ticket_repository, :org_repository
      attr_accessor :repository, :field_name, :value

      def search(field_name:, value:)
        repository.search(
          field_name: field_name,
          value: value
        )
      end

      def init_menus
        @home_menu = UserInterfaces::HomeMenu.new
        @repository_menu = UserInterfaces::RepositoryMenu.new
        @search_menu = UserInterfaces::SearchMenu.new
        @list_menu = UserInterfaces::ListMenu.new
      end

      def init_repositories
        @user_repository = Repositories::Repository.new(
          resource: "#{Dir.pwd}/docs/users.json"
        ).load
        @ticket_repository = Repositories::Repository.new(
          resource: "#{Dir.pwd}/docs/tickets.json"
        ).load
        @org_repository = Repositories::Repository.new(
          resource: "#{Dir.pwd}/docs/organizations.json"
        ).load
      end

      def settings
        @settings ||= Config.settings
      end

      def repositories
        {
          Users: user_repository,
          Tickets: ticket_repository,
          Organizations: org_repository
        }
      end

    end
  end
end
