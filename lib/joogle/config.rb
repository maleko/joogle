require 'yaml'
require 'active_support/core_ext/hash/indifferent_access'

module Joogle
  module Config
    class << self

      def settings
        @settings || load_yaml
      end

      private

      def load_yaml
        @settings ||= YAML.load(config_file)
      end

      def config_file
        raise "config/menu.yml config file missing" unless File.exists?('config/menu.yml')
        @config_file ||= File.read("config/menu.yml")
      end

    end
  end
end
