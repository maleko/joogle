require 'tty'

module Joogle
  module UserInterfaces
    class Base
      def say(description:)
        interface.say(description)
      end

      def prompt(description:, choices:)
        interface.expand(description, choices)
      end

      def select(description:, choices:)
        interface.select(description, choices)
      end

      def ask(description:)
        interface.ask(description)
      end

      private

      def interface
        @interface ||= TTY::Prompt.new
      end
    end
  end
end
