# Joogle

[![CircleCI](https://circleci.com/bb/maleko/joogle.svg?style=svg)](https://circleci.com/bb/maleko/joogle)

Yet another Zendesk code challenge

## Installation

The application assumes you have installed Ruby v2.4.0 and the bundler gem.

To install, run the following:

    $ bin/setup

## Usage

Run it by executing: 

	$ exe/joogle
	
Follow the prompts
