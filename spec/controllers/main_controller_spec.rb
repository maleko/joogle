require 'spec_helper'

module Joogle
  module Controllers
    RSpec.describe MainController do

      let(:controller) { described_class.new }
      let(:repository) { instance_double Repositories::Repository, list_field_names: list_field_names, search: [] }
      let(:list_field_names) { ["test"] }

      describe '.new' do
        subject { controller }

        before do
          allow(repository).to receive(:load).and_return repository
        end

        [UserInterfaces::HomeMenu, UserInterfaces::RepositoryMenu, UserInterfaces::SearchMenu, UserInterfaces::ListMenu].each do |menu|
          it "initialises the menu '#{menu}'" do
            expect(menu).to receive(:new)
            subject
          end
        end

        it 'initialises the repositories' do
          expect(Repositories::Repository).to receive(:new).exactly(3).times.and_return repository
          subject
        end
      end

      describe '#display_home_menu' do
        let(:home_menu) { instance_double UserInterfaces::HomeMenu, prompt: 'search' }

        subject { controller.display_home_menu }

        before do
          allow(UserInterfaces::HomeMenu).to receive(:new).and_return home_menu
          allow(controller).to receive(:display_repository_menu)
        end

        it 'displays the home menu' do
          subject
          expect(home_menu).to have_received(:prompt)
        end

        context 'search decision' do
          it 'displays the repository menu' do
            subject
            expect(controller).to have_received(:display_repository_menu)
          end
        end

        context 'list decision' do
          before do
            allow(home_menu).to receive(:prompt).and_return 'list'
            allow(controller).to receive(:display_list_menu)
          end

          it 'display the list' do
            subject
            expect(controller).to have_received(:display_list_menu)
          end
        end
      end

      describe '#display_repository_menu' do
        let(:repository_menu) { instance_double UserInterfaces::RepositoryMenu, display_repositories_list: repository }

        subject { controller.display_repository_menu }

        before do
          allow(UserInterfaces::RepositoryMenu).to receive(:new).and_return repository_menu
          allow(controller).to receive(:display_search_menu)
        end

        it 'displays the repositories list' do
          subject
          expect(repository_menu).to have_received(:display_repositories_list)
        end

        it 'calls the next stage' do
          subject
          expect(controller).to have_received(:display_search_menu)
        end
      end

      describe '#display_search_menu' do
        let(:search_menu) { instance_double UserInterfaces::SearchMenu, display_search_term: 'tag', display_search_value: 'Maine' }

        subject { controller.display_search_menu }

        before do
          allow(UserInterfaces::SearchMenu).to receive(:new).and_return search_menu
          allow(controller).to receive(:repository).and_return(repository)
          allow(controller).to receive(:display_search_result)
        end

        it 'displays the search term' do
          subject
          expect(search_menu).to have_received(:display_search_term)
        end

        it 'displays the search value' do
          subject
          expect(search_menu).to have_received(:display_search_value)
        end

        it 'calls the next stage' do
          subject
          expect(controller).to have_received(:display_search_result)
        end
      end

      describe '#display_search_result' do
        let(:search_menu) { instance_double UserInterfaces::SearchMenu, display_search_result: nil }
        let(:field_name)  { 'tag' }
        let(:value) { 'Maine' }

        subject { controller.display_search_result }

        before do
          allow(UserInterfaces::SearchMenu).to receive(:new).and_return search_menu
          allow(controller).to receive(:repository).and_return(repository)
          allow(controller).to receive(:display_home_menu)
          allow(controller).to receive(:field_name).and_return field_name
          allow(controller).to receive(:value).and_return value
        end

        it 'searches the repository' do
          subject
          expect(repository).to have_received(:search).with(field_name: field_name,  value: value)
        end

        it 'displays the search result' do
          subject
          expect(search_menu).to have_received(:display_search_result)
        end

        it 'redirects to the home menu' do
          subject
          expect(controller).to have_received(:display_home_menu)
        end
      end

      describe '#display_list_menu' do
        let(:list_menu) { instance_double UserInterfaces::ListMenu, list_fields: nil }

        subject { controller.display_list_menu }

        before do
          allow(UserInterfaces::ListMenu).to receive(:new).and_return list_menu
          allow(controller).to receive(:repositories).and_return({ Users: repository })
          allow(controller).to receive(:display_home_menu)
        end

        it 'lists the fields available to the repository' do
          subject
          expect(list_menu).to have_received(:list_fields)
        end

        it 'redirects to the home menu' do
          subject
          expect(controller).to have_received(:display_home_menu)
        end
      end
    end
  end
end
