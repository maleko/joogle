require 'spec_helper'

module Joogle
  module UserInterfaces
    RSpec.describe HomeMenu do
      let(:base) { instance_double Base, prompt: true }
      let(:description) { 'ipsum lorem' }
      let(:choices) { [{}] }
      let(:home_menu) { described_class.new }

      describe '#prompt' do
        before do
          allow(Base).to receive(:new).and_return base
        end

        it 'prompts with the description and settings from settings by default' do
          home_menu.prompt(description: description, choices: choices)
          expect(base).to have_received(:prompt).with(description: description, choices: choices)
        end

        context 'modified params' do
          let(:new_description) { 'lorem ipsum' }
          let(:new_choices) { [{ key: "1" }] }

          it 'accepts modified choices' do
            home_menu.prompt(description: new_description, choices: new_choices)
            expect(base).to have_received(:prompt).with(description: new_description, choices: new_choices)
          end
        end
      end
    end
  end
end
