require 'spec_helper'

module Joogle
  module UserInterfaces
    RSpec.describe RepositoryMenu do
      let(:base) { instance_double Base, select: answer }
      let(:answer) { 'whatever' }
      let(:description) { 'Lorem ipsum dolor sit amet' }
      let(:choices) { [{}] }
      let(:repository_menu) { described_class.new }

      before do
        allow(Base).to receive(:new).and_return base
      end

      describe '#display_repositories_list' do
        subject { repository_menu.display_repositories_list(description: description, choices: choices) }

        it 'presents a selection based on the choices provided' do
          subject
          expect(base).to have_received(:select).with(description: description, choices: choices)
        end

        it 'returns the answer' do
          is_expected.to eq answer
        end
      end
    end
  end
end
