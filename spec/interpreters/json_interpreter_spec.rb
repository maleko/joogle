require 'spec_helper'

module Joogle
  module Interpreters
    RSpec.describe JSONInterpreter do
      let(:interpreter) { described_class.new }
      let(:filename) { "#{Dir.pwd}/spec/fixtures/users.json" }
      let(:contents) { '[ { "test": "a" }, { "test": "b" } ]' }

      describe "#parse" do

        subject { interpreter.parse(filename: filename) }

        before do
          allow(File).to receive(:read).and_return(contents)
          allow(JSON).to receive(:parse)
        end

        it 'retrieves the contents of the file' do
          subject
          expect(File).to have_received :read
        end

        it 'parses the contents' do
          subject
          expect(JSON).to have_received(:parse).with(contents, symbolize_names: true)
        end
      end
    end
  end
end
