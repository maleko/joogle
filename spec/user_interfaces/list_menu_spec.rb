require 'spec_helper'

module Joogle
  module UserInterfaces
    RSpec.describe ListMenu do
      let(:base) { instance_double Base, say: nil}
      let(:description) { 'test' }
      let(:fields) { %w{ lorem ipsum dolor sit amet } }
      let(:list_menu) { described_class.new }

      before do
        allow(Base).to receive(:new).and_return base
      end

      describe '#list_fields' do
        let(:expected_result) do
          "\n" +
          ('-' * 10) + "\n" +
          description + "\n" +
          ('-' * 10) + "\n" +
          fields.join("\n")
        end

        subject { list_menu.list_fields(description: description, fields: fields) }

        it 'presents a question' do
          subject
          expect(base).to have_received(:say).with(description: expected_result)
        end
      end
    end
  end
end
